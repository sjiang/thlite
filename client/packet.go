package client

import (
	"bytes"
	"compress/zlib"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io/ioutil"
)

type Packet struct {
	Opt      int
	Cmd      int
	Len      int
	contents bytes.Buffer
}

const (
	UInt   uint32 = 0
	UShort uint16 = 0
	UByte  uint8  = 0
	Int    int32  = 0
	Short  int16  = 0
	Byte   int8   = 0
)

var seq = 1

func (p *Packet) Reset() *Packet {

	seq = seq + 1
	if seq > 65535 {
		seq = 1
	}
	p.Opt = 0
	p.Cmd = 0
	p.Len = 0
	p.contents.Reset()

	return p
}

func (p *Packet) GetLen() int { return p.Len }

func (p *Packet) SetOpt(opt int) { p.Opt = opt }

func (p *Packet) SetCmd(cmd int) { p.Cmd = cmd }

func (p *Packet) Write(b []byte) {
	_, _ = p.contents.Write(b)
	p.Len = p.Len + len(b)
}

func NumToBytes(i int, iType interface{}) []byte {
	buf := new(bytes.Buffer)
	switch iType.(type) {
	case int32:
		b := int32(i)
		binary.Write(buf, binary.LittleEndian, b)
	case int16:
		b := int16(i)
		binary.Write(buf, binary.LittleEndian, b)
	case int8:
		b := int8(i)
		binary.Write(buf, binary.LittleEndian, b)
	case uint32:
		b := uint32(i)
		binary.Write(buf, binary.LittleEndian, b)
	case uint16:
		b := uint16(i)
		binary.Write(buf, binary.LittleEndian, b)
	case uint8:
		b := uint8(i)
		binary.Write(buf, binary.LittleEndian, b)
	default:
		b := int8(0)
		binary.Write(buf, binary.LittleEndian, b)
	}
	return buf.Bytes()
}

func StrToBytes(s string) []byte {
	var buffer bytes.Buffer
	buffer.Write(NumToBytes(len(s), UShort))

	buffer.WriteString(s)

	return buffer.Bytes()
}

func BytesToNum(b []byte, iType interface{}) (i int, err error) {
	buf := bytes.NewReader(b)

	switch iType.(type) {
	case int32:
		temp := int32(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	case int16:
		temp := int16(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	case int8:
		temp := int8(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	case uint32:
		temp := uint32(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	case uint16:
		temp := uint16(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	case uint8:
		temp := uint8(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	default:
		temp := int8(0)
		err = binary.Read(buf, binary.LittleEndian, &temp)
		i = int(temp)
	}
	return
}

func BytesToStr(b []byte, i int) (string, int) {
	length, _ := BytesToNum(b[i:i+2], UShort)

	return string(b[i+2 : i+2+length]), i + 2 + length
}

func CompressedToBytes(b []byte) (uncompressed []byte, err error) {
	reader := bytes.NewReader(b)
	zlibReader, err := zlib.NewReader(reader)
	if err != nil {
		return
	}
	uncompressed, err = ioutil.ReadAll(zlibReader)
	if err != nil {
		return
	}
	return
}

func ConcatStrings(strings ...string) string {
	var buffer bytes.Buffer

	for i := 0; i < len(strings); i++ {
		buffer.WriteString(strings[i])
	}

	return buffer.String()
}

func (p *Packet) GetBytes() []byte {
	var buffer bytes.Buffer
	buffer.Write(NumToBytes(seq, UShort))
	buffer.Write(NumToBytes(p.Opt, UShort))
	buffer.Write(NumToBytes(p.Cmd, UShort))
	buffer.Write(NumToBytes(p.Len, UShort))
	buffer.Write(p.contents.Bytes())

	return buffer.Bytes()
}

func (p *Packet) Flush() []byte {
	b := p.GetBytes()
	p.Reset()

	return b
}

func (p Packet) String() string {
	return fmt.Sprintf("Pkt%d Opt%d Cmd%d Len%d\n%s", seq, p.Opt, p.Cmd, p.Len, hex.Dump(p.GetBytes()))
}
