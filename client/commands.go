package client

import (
	"fmt"
)

var idsToCommand = map[int]string{
	1:    "INVALID",
	6:    "SYSTEM_CHAT",
	7:    "CMD_LINE",
	8:    "MESSAGE_BOX",
	9:    "CHAT",
	10:   "LOGIN",
	11:   "QUERY_XML",
	12:   "PLAYER_USERNAME_GET",
	13:   "CITY_USERNAME_GET",
	14:   "PLAYER_NAME_FROM_CITY_NAME",
	15:   "TRIBE_USERNAME_GET",
	16:   "STRONGHOLD_USERNAME_GET",
	20:   "PLAYER_PROFILE",
	21:   "PLAYER_DESCRIPTION_SET",
	22:   "PROFILE_BY_TYPE",
	23:   "SAVE_TUTORIAL_STEP",
	24:   "SAVE_MUTE_SOUND",
	25:   "PLAYER_COINS_UPDATE",
	26:   "PLAYER_THEME_PURCHASED",
	51:   "ACTION_CANCEL",
	52:   "ACTION_COMPLETE",
	53:   "ACTION_START",
	54:   "ACTION_RESCHEDULE",
	61:   "NOTIFICATION_ADD",
	62:   "NOTIFICATION_REMOVE",
	63:   "NOTIFICATION_UPDATE",
	64:   "NOTIFICATION_LOCATE",
	65:   "MESSAGE_UNREAD",
	66:   "BATTLE_REPORT_UNREAD",
	67:   "FORUM_UNREAD",
	68:   "REFRESH_UNREAD",
	71:   "REFERENCE_ADD",
	72:   "REFERENCE_REMOVE",
	102:  "REGION_ROAD_DESTROY",
	103:  "REGION_ROAD_BUILD",
	104:  "REGION_SET_TILE",
	105:  "REGION_GET",
	106:  "MINIMAP_REGION_GET",
	201:  "OBJECT_ADD",
	202:  "OBJECT_UPDATE",
	203:  "OBJECT_REMOVE",
	204:  "OBJECT_MOVE",
	300:  "STRUCTURE_INFO",
	301:  "STRUCTURE_BUILD",
	302:  "STRUCTURE_UPGRADE",
	303:  "STRUCTURE_CHANGE",
	304:  "STRUCTURE_LABOR_MOVE",
	305:  "STRUCTURE_DOWNGRADE",
	306:  "STRUCTURE_SELF_DESTROY",
	307:  "STRUCTURE_SET_THEME",
	308:  "WALL_SET_THEME",
	309:  "ROAD_SET_THEME",
	311:  "TECHNOLOGY_ADDED",
	312:  "TECHNOLOGY_UPGRADE",
	313:  "TECHNOLOGY_REMOVED",
	314:  "TECHNOLOGY_UPGRADED",
	315:  "TECHNOLOGY_CLEARED",
	451:  "CITY_OBJECT_ADD",
	452:  "CITY_OBJECT_UPDATE",
	453:  "CITY_OBJECT_REMOVE",
	460:  "CITY_RESOURCES_SEND",
	462:  "CITY_RESOURCES_UPDATE",
	463:  "CITY_UNIT_LIST",
	464:  "CITY_LOCATE_BY_NAME",
	465:  "CITY_RADIUS_UPDATE",
	466:  "CITY_LOCATE",
	467:  "CITY_POINTS_UPDATE",
	468:  "CITY_HIDE_NEW_UNITS_UPDATE",
	469:  "CITY_HAS_AP_BONUS",
	470:  "CITY_DEFAULT_THEME_UPDATE",
	471:  "CITY_TROOP_THEME_UPDATE",
	490:  "CITY_BATTLE_STARTED",
	491:  "CITY_BATTLE_ENDED",
	497:  "CITY_NEW_UPDATE",
	498:  "CITY_CREATE",
	499:  "CITY_CREATE_INITIAL",
	350:  "FOREST_INFO",
	351:  "FOREST_CAMP_CREATE",
	352:  "FOREST_CAMP_REMOVE",
	501:  "UNIT_TRAIN",
	502:  "UNIT_UPGRADE",
	503:  "UNIT_TEMPLATE_UPGRADED",
	600:  "TROOP_INFO",
	603:  "TROOP_RETREAT",
	611:  "TROOP_ADDED",
	612:  "TROOP_UPDATED",
	613:  "TROOP_REMOVED",
	614:  "TROOP_ATTACK_CITY",
	615:  "TROOP_ATTACK_STRONGHOLD",
	616:  "TROOP_REINFORCE_CITY",
	617:  "TROOP_REINFORCE_STRONGHOLD",
	618:  "TROOP_ATTACK_BARBARIAN_TRIBE",
	619:  "TROOP_SWITCH_MODE",
	620:  "TROOP_TRANSFER",
	621:  "LOCAL_TROOP_MOVE",
	622:  "TROOP_SET_THEME",
	700:  "BATTLE_SUBSCRIBE",
	701:  "BATTLE_UNSUBSCRIBE",
	702:  "BATTLE_ATTACK",
	703:  "BATTLE_REINFORCE_ATTACKER",
	704:  "BATTLE_REINFORCE_DEFENDER",
	705:  "BATTLE_ENDED",
	706:  "BATTLE_SKIPPED",
	707:  "BATTLE_NEW_ROUND",
	708:  "BATTLE_WITHDRAW_ATTACKER",
	709:  "BATTLE_WITHDRAW_DEFENDER",
	710:  "BATTLE_GROUP_UNIT_ADDED",
	711:  "BATTLE_GROUP_UNIT_REMOVED",
	712:  "BATTLE_PROPERTIES_UPDATED",
	801:  "RESOURCE_GATHER",
	901:  "MARKET_BUY",
	902:  "MARKET_SELL",
	903:  "MARKET_PRICES",
	1001: "TRIBE_INFO",
	1002: "TRIBE_CREATE",
	1003: "TRIBE_DELETE",
	1004: "TRIBE_UPDATE",
	1005: "TRIBE_UPGRADE",
	1006: "TRIBE_DESCRIPTION_SET",
	1008: "TRIBE_TRANSFER",
	1009: "TRIBE_INFO_BY_NAME",
	1010: "TRIBE_UPDATE_RANK",
	1012: "TRIBESMAN_REMOVE",
	1013: "TRIBESMAN_UPDATE",
	1014: "TRIBESMAN_REQUEST",
	1015: "TRIBESMAN_CONFIRM",
	1016: "TRIBESMAN_SET_RANK",
	1017: "TRIBESMAN_LEAVE",
	1018: "TRIBESMAN_CONTRIBUTE",
	1019: "TRIBESMAN_GOT_KICKED",
	1022: "TRIBE_CITY_ASSIGNMENT_CREATE",
	1023: "TRIBE_ASSIGNMENT_JOIN",
	1024: "TRIBE_STRONGHOLD_ASSIGNMENT_CREATE",
	1025: "TRIBE_ASSIGNMENT_EDIT",
	1026: "TRIBE_ASSIGNMENT_REMOVE_TROOP",
	1031: "TRIBE_UPDATE_NOTIFICATIONS",
	1051: "TRIBE_UPDATE_CHANNEL",
	1052: "TRIBE_RANKS_UPDATE_CHANNEL",
	1101: "STRONGHOLD_INFO",
	1102: "STRONGHOLD_PUBLIC_INFO_BY_NAME",
	1103: "STRONGHOLD_LOCATE",
	1104: "STRONGHOLD_GATE_REPAIR",
	1105: "STRONGHOLD_LOCATE_BY_NAME",
	1106: "STRONGHOLD_LIST",
	1107: "STRONGHOLD_SET_THEME",
	1201: "STORE_GET_ITEMS",
	1202: "STORE_PURCHASE_ITEM",
	1203: "STORE_SET_DEFAULT_THEME",
	1204: "STORE_THEME_APPLY_TO_ALL",
	1205: "STORE_SET_TROOP_THEME"}

var commandsToId = map[string]int{}

func init() {

	for k, v := range idsToCommand {
		commandsToId[v] = k
	}
}

func GetCommand(id int) (command string, err error) {
	if val, ok := idsToCommand[id]; ok {
		command = val
	} else {
		err = fmt.Errorf("Could not find command for %d", id)
	}

	return
}

func GetCommandID(command string) (id int, err error) {
	if val, ok := commandsToId[command]; ok {
		id = val
	} else {
		err = fmt.Errorf("Could not find command for %d", command)
	}

	return
}
