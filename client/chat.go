package client

import (
	"bufio"
	"log"
	"net"
	"os"
)

func ReadChat(b []byte) (userName string, chat string, channel_type int, err error) {
	channel_type, err = BytesToNum(b[0:1], UByte)
	if err != nil {
		return
	}

	userName, messageInd := BytesToStr(b, 9)

	chat, _ = BytesToStr(b, messageInd)

	return
}

func WriteChat(str string) []byte {
	return StrToBytes(str)

}

func GetChat(conn net.Conn, p *Packet, verbose bool) {
	for {
		buf := bufio.NewReader(os.Stdin)
		chat, err := buf.ReadBytes('\n')
		if err != nil {
			log.Println(err)
		} else {
			cmd, err := GetCommandID("CHAT")
			if err != nil {
				log.Println(err)
				continue
			}
			p.Reset()
			p.SetCmd(cmd)
			p.Write(NumToBytes(0, UByte))
			p.Write(StrToBytes(string(chat)))
			if verbose {
				log.Println("Sending packet:\n", p)
			}
			conn.Write(p.Flush())
		}
	}

}
