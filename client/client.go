package client

import (
	"bufio"
	"encoding/hex"
	"log"
	"net"
)

func Read(conn net.Conn, verbose bool, finished chan bool) {
	var (
		sequence, option, command, length int
		err                               error
	)
	connBuffer := bufio.NewReader(conn)
	header := make([]byte, 8)
	var content []byte
	var b byte

	for {
		dump, _ := connBuffer.Peek(8)
		if verbose {
			log.Printf("Checking if logged out:%v\n", dump)
		}
		if len(dump) == 0 {
			log.Print("Connection lost. Please reconnect")
			finished <- true
		}
		for i := 0; i < 8; i++ {
			for {
				b, err = connBuffer.ReadByte()
				if err == nil {
					break
				}
			}
			header[i] = b
		}

		if verbose {
			log.Printf("Header: \n%s", hex.Dump(header))
		}

		sequence, err = BytesToNum(header[0:2], UShort)
		if err != nil {
			log.Printf("error reading sequence %s", err)
			finished <- true
		}
		option, err = BytesToNum(header[2:4], UShort)
		if err != nil {
			log.Printf("error reading option %s", err)
			finished <- true
		}
		command, err = BytesToNum(header[4:6], UShort)
		if err != nil {
			log.Printf("error reading command %s", err)
			finished <- true
		}
		length, err = BytesToNum(header[6:8], UShort)
		if err != nil {
			log.Printf("error reading length %s", err)
			finished <- true
		}
		if length > 0 {
			content = make([]byte, length)

			dump, _ := connBuffer.Peek(1)
			if len(dump) == 0 {
				log.Fatalln("Error receiving packet content")
			}
			if verbose {
				log.Printf("%v\n", dump)
			}

			for i := 0; i < length; i++ {
				for {
					b, err = connBuffer.ReadByte()
					if err == nil {
						break
					}
				}
				content[i] = b
			}
		}
		commandStr, err := GetCommand(command)
		if err != nil {
			log.Print(err)
			continue
		}
		if option&4 == 4 {
			content, err = CompressedToBytes(content)
		}
		if commandStr == "LOGIN" {
			log.Print("Logged in successfully.\n")
		}
		if verbose {
			log.Printf("Message Received:\n\n%s\nPkt%d Opt%d Len%d\n%s\n", commandStr, sequence, option, length, hex.Dump(content))
		} else {
			//log.Printf(commandStr)
		}
		if commandStr == "CHAT" {
			userName, chat, channel_type, err := ReadChat(content)
			if err != nil {
				log.Print(err)
			}
			log.Printf("%d %s: %s\n%s", channel_type, userName, chat, "\x07")
		}
	}
}
