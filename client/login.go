package client

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"regexp"
	"time"
)

func EnterCredentials() (username string, password string) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("\n\nPlease Enter Username:")
	scanner.Scan()
	username = scanner.Text()
	fmt.Println("\n\nPlease Enter Password:")
	scanner.Scan()
	password = scanner.Text()

	return username, password
}

func GetTHServerCred(loginurl string, appurl string, user string, pass string) (hostname string, sessionKey string, version string, err error) {
	timeout := time.Duration(30 * time.Second)

	//initializing to get initial cookie info (no login)
	cookieJar, _ := cookiejar.New(nil)
	u, _ := url.Parse(loginurl)
	initToken := ""

	client := http.Client{
		Timeout: timeout,
		Jar:     cookieJar,
	}

	response, err := client.Get(loginurl)
	if err != nil {
		return "", "", "", errors.New("login: Error reaching login page")
	}

	defer response.Body.Close()

	for _, cookie := range client.Jar.Cookies(u) {
		if cookie.Name == "CAKEPHP" {
			initToken = cookie.Value
		}
	}

	//logging into TH
	values := url.Values{}
	values.Set("data[Player][name]", user)
	values.Add("data[Player][password]", pass)
	// values.Add("data[Player][auto_login]", "0")
	values.Add("data[Player][auto_login]", "1")

	response, err = client.PostForm(loginurl, values)
	if err != nil {
		return "", "", "", errors.New("login: Error sending login information")
	}

	defer response.Body.Close()

	//when a user logs in, the CAKEPHP token changes - using this to determine successful login
	updateToken := initToken

	for _, cookie := range client.Jar.Cookies(u) {
		if cookie.Name == "CAKEPHP" {
			updateToken = cookie.Value
		}
	}

	if initToken != updateToken {
		// log.Println("Login successful!")
	} else {
		// log.Println("Login failed!")
		return "", "", "", errors.New("login: Login unsuccessful (bad password?)")
	}

	response, err = client.Get(appurl)
	if err != nil {
		return "", "", "", errors.New("login: Error reaching app page")
	}

	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", "", "", errors.New("login: Could not read response from TH server")
	}

	reSession := regexp.MustCompile("lsessid: \"(.+)\"")
	sessionKey = string(reSession.FindSubmatch(body)[1])

	reHostname := regexp.MustCompile("hostname: \"(.+)\"")
	hostname = string(reHostname.FindSubmatch(body)[1])

	reVersion := regexp.MustCompile("siteVersion: \"(.+)\"")
	version = string(reVersion.FindSubmatch(body)[1])

	if sessionKey == "" {
		return "", "", "", errors.New("login: Could not find session key")
	}

	return hostname, sessionKey, version, nil
}
