package main

import (
	"flag"
	"io"
	"log"
	"net"
	"os"
	"thlite/client"
)

func main() {
	var (
		username, password, hostname, sessionkey, version string
		err                                               error
	)

	verbosePtr := flag.Bool("v", false, "Verbose mode")
	flag.Parse()

	verbose := *verbosePtr

	logFile, err := os.OpenFile("log.log", os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer logFile.Close()

	mw := io.MultiWriter(os.Stdout, logFile)
	log.SetOutput(mw)

	for {
		username, password = client.EnterCredentials()

		hostname, sessionkey, version, err = client.GetTHServerCred("http://tribalhero.com/players/login", "http://tribalhero.com/play/tribblez/v:95.0", username, password)
		if err == nil {
			break
		} else {
			log.Println(err)
		}
	}
	if verbose {
		log.Println(hostname)
		log.Println(sessionkey)
		log.Println(version)
	}

	packet := client.Packet{}
	packet.SetCmd(10)
	packet.Write(client.NumToBytes(999, client.UShort))
	packet.Write(client.NumToBytes(999, client.UShort))
	packet.Write(client.NumToBytes(0, client.UByte))
	packet.Write(client.StrToBytes(username))
	packet.Write(client.StrToBytes(sessionkey))
	if verbose {
		log.Println("Sending packet:\n", packet)
	}

	conn, err := net.Dial("tcp", client.ConcatStrings(hostname, ":443"))
	if err != nil {
		log.Fatalln(err)
	}
	conn.Write(packet.Flush())
	finished := make(chan bool)
	go client.Read(conn, verbose, finished)
	go client.GetChat(conn, &packet, verbose)
	<-finished
	log.Println("Done.")

}
